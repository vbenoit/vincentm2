################################################################################
### Description
# Auteurs: Matthieu AUTHIER, Vincent BENOIT
# Dernière modif: 2023-07-04
# R version 4.2.3 (2023-03-15 ucrt) -- "Shortstop Beagle"
# Copyright (C) 2023 The R Foundation for Statistical Computing
# Platform: x86_64-w64-mingw32/x64 (64-bit)
#
################################################################################

### Generating "distance data" from a scientific campaign using the sismow package

library(move)
library(tidyverse)
library(sismow)
library(sf)
library(sfheaders)
library(units)


# Making the results reproducible
set.seed(1)

# Projection
crs = 3035

# Area Map
shape_obj = bob

# Abundance Map
map_obj = gen.abundance

# Truncation value
truncation = 1000


### SIMULATE INDIVIDUALS

N_ind <- 10000        # number of groups to generate
group_size = 5        # average number of individuals in a group
ind <- simulate_ind(map_obj = map_obj,
                    mean_group_size = group_size,
                    N = N_ind,
                    crs=crs
                    )

# Total number of generated individuals
gen.ind <- ind %>% 
  pull(size) %>% 
  sum()

# Normalizing abundance to match number of generated individuals
gen.abundance$abundance <- gen.abundance$abundance*(N_ind*group_size)/sum(gen.abundance$abundance)


### Plot Abundance Map
abundance_map <- gen.abundance %>%
  ggplot() +
  geom_sf(data = france, fill = "grey", alpha = 0.3, linewidth = 0.25) +
  geom_sf(aes(fill = abundance), color = NA) +
  coord_sf(xlim = c(2900000, 3500000), ylim = c(2300000, 3150000), expand = FALSE) +
  scale_fill_viridis_c(trans = "sqrt", name='Abundance') +
  theme_bw()

print(abundance_map)

### SIMULATE TRANSECTS

transects <- simulate_transects(shape_obj = shape_obj,
                                design = "eszigzagcom",
                                line_length = drop_units(5*st_area(shape_obj)/(100*truncation)),
                                design_angle = 0,
                                segmentize = TRUE,
                                seg_length = 5000,
                                crs=crs
                                )


### SIMULATE OBSERVATIONS

obs <- simulate_obs(ind_obj = ind,
                    transect_obj = transects,
                    key = "hn",
                    g_zero = 1,
                    esw = 200,
                    truncation = truncation,
                    crs = crs
                    )


### DETECTED

detected <- filter(obs, detected == 1)
