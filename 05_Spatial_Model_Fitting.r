################################################################################
### Description
# Auteurs: Matthieu AUTHIER, Vincent BENOIT
# Dernière modif: 2023-07-04
# R version 4.2.3 (2023-03-15 ucrt) -- "Shortstop Beagle"
# Copyright (C) 2023 The R Foundation for Statistical Computing
# Platform: x86_64-w64-mingw32/x64 (64-bit)
#
################################################################################

### Spatial model and abundance prediction with the selected model
# Following the method presented here: http://distancesampling.org/R/vignettes/mexico-analysis.html

library(move)
library(tidyverse)
library(sf)
library(units)
library(dsm)
library(Distance)
library(rlist)
library(gridExtra)


### Fitting DSM

# DSM with the best model
dsm.xy.final <- dsm(as.formula(best),
                    detfc.hn.null,
                    segdata,
                    obsdata,
                    method="REML",
                    family = nb()
                    )

summary(dsm.xy.final)

# Relationship between the covariates and the linear predictor
library(gratia)
theme_set(theme_bw())
covariable_fit <- gratia::draw(dsm.xy.final)
#print(covariable_fit)

# gen_splines <- gen.density$splines %>%
#   ggplot(aes(x = cov_value, y = y_value, color = cov_name)) +
#   geom_line() +
#   facet_wrap(~cov_name, ncol = 1, scales = "free") +
#   scale_color_viridis_d(name = "Covariate") +
#   theme_bw()
# print(gen_splines)


### Predicting abundance map

dsm.xy.final.pred <- predict(dsm.xy.final, preddata, preddata$area)

prediction.final = data.frame(
  ID = preddata$ID,
  x = preddata$x,
  y = preddata$y,
  abundance = dsm.xy.final.pred,
  geometry = preddata$geom
  ) %>%
  drop_na()  # dropping lines with NAs

# Dropping the lines in gen.abundance that where dropped in prediction.final (because of NAs)
gen.abundance.final <- left_join(data.frame("ID"=prediction.final$ID), gen.abundance, by="ID")


### Comparing generated abundance and predicted abundance

## Maps
# Computing error for each cell
prediction.final$difference = prediction.final$abundance - gen.abundance.final$abundance
prediction.final$error.pct = abs(gen.abundance.final$abundance - prediction.final$abundance)*100/gen.abundance.final$abundance

# Cleaning environment
rm('detfc.hn.null','dsm.xy.final','obsdata','preddata','segdata','dsm.xy.final.pred','covariable_fit')
