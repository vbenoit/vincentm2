

lapply(c("tidyverse", "purrr", "sf", "units", "sismow", "mgcv","rnaturalearth","ggforce","pelarrp"),
       library, character.only = TRUE
)



### Calculate population P within the circle of radius r and center (x, y)
pop_within <- function(abundance,
                       x0,
                       y0,
                       r0) {
  P = 0
  id <- rep(0, nrow(abundance))
  for (i in 1:length(abundance$x)) {
    d = sqrt((abundance$x[i]-x0)^2+(abundance$y[i]-y0)^2)
    if (d <= r0) {
      P = P + abundance$abundance[i]
      id[i] <- 1
    }
  }
  return(P[[1]])
}

st_pop_within <- function(abundance_sf, 
                          x0 = 3205138, 
                          y0 = 2703990, 
                          r0 = 1E5, 
                          crs_proj = 3035
                          ) {
  # point geometry for center of circle
  center_sf <- data.frame(id = 0, x = x0, y = y0) %>%
    st_as_sf(., coords = c("x", "y"), crs = crs_proj) %>%
    st_cast()
  # identify cells within the circle
  is_within <- st_is_within_distance(x = abundance_sf %>%
                                       st_centroid(), 
                                     y = center_sf, 
                                     dist = units::set_units(r0, 'm')
                                     ) %>%
    lapply(., function(x) {ifelse(length(x) == 0, 0, 1)}) %>%
    unlist()
  # sum abundance
  inside_circle <- abundance_sf %>%
    st_drop_geometry() %>%
    mutate(id = 1:n()) %>%
    filter(id %in% which(is_within == 1)) %>%
    pull(abundance) %>%
    sum()
  return(inside_circle)
}


### Binary search to find the minimum radius r where the population within the circle of center (x,y) and radius r is ≥ target_pop
binary_search <- function(abundance,
                          x,
                          y,
                          target_pop
                          ) {
  
  # Find the maximum distance between two points in the grid
  points = rbind(abundance$x, abundance$y)
  distances <- dist(t(points), method = "euclidean")
  
  r_max <- max(distances)
  r_min <- 0
  
  # Setting threshold for the search
  eps <- min(distances)/20
  
  while (r_max - r_min >= eps) {
    r = (r_min + r_max) / 2
    P = pop_within(abundance, x, y, r)
    
    # Update rmin or rmax with r
    if (P < target_pop){
      r_min <- r
    } else {
      r_max <- r
    }
  }
  return(r_max)
}

st_binary_search <- function(abundance_sf,
                             x,
                             y,
                             target_pop
                             ) {
  # Find the maximum distance between two points in the grid
  distances <- abundance_sf %>%
    st_centroid() %>%
    st_coordinates() %>%
    dist(., method = "euclidean")
  
  r_max <- max(distances)
  r_min <- 0
  
  # Setting threshold for the search
  eps <- min(distances) / 20
  
  while (r_max - r_min >= eps) {
    r = (r_min + r_max)/2
    P = st_pop_within(abundance_sf = abundance_sf, x0 = x, y0 = y, r0 = r)
    
    # Update rmin or rmax with r
    if (P < target_pop){
      r_min <- r
    } else {
      r_max <- r
    }
  }
  return(r_max)
  
}

### Finding the valeriepieris circle(s) containing at least a fraction f of the population of a given abundance map
valeriepieris_circle <- function(abundance, 
                                 f = 0.5,
                                 crs = 3035) {
  
  # Calculating target_pop
  target_pop <- f*sum(abundance$abundance)
  target_pop <- trunc(target_pop*10^3)/10^3   # Truncate to avoid rounding errors
  
  # Finding the minimum radius r_v where the population within the circle centered on the 1st point is ≥ target_pop 
  r_v <- binary_search(abundance, abundance$x[1], abundance$y[1], target_pop)
  x_v = abundance$x[1]
  y_v = abundance$y[1]
  
  #Looping over every other point in the grid map
  for (i in 2:length(abundance$x)) {
    
    # Calculating the population within the circle centered on the current point and of radius r_v
    pop = pop_within(abundance, abundance$x[i],abundance$y[i],r_v)
    
    # If still exceeding target population
    if (pop >= target_pop){
      # Calculate the new smallest radius r_v such that the circle centered on the current point 
      # and of radius r_v has a population ≥ target_pop
      r_v <- binary_search(abundance, abundance$x[i], abundance$y[i], target_pop)
      
      # Update coordinates of the center
      x_v = abundance$x[i]
      y_v = abundance$y[i]
    }
  }
  
  v_circle = data.frame(x_coordinate = x_v, y_coordinate = y_v, radius = r_v, what = "valeriepieris")
  
  # Create Valeriepieris Circle object with st_buffer
  point_sf <- v_circle %>% 
    st_as_sf(., coords = c("x_coordinate", "y_coordinate"), 
             crs = crs
             ) %>%
    # st_buffer(dist = units::set_units(v_circle$radius, "m")) %>%
    st_cast()
  
  return(list(center = point_sf, radius = v_circle$radius))
}



### Find Weighted Geometric Median using Weiszfeld's algorithm
geomedian <- function(x,
                      y,
                      weights = rep(1, length(x)),
                      epsilon = 1e-6,
                      crs = 3035
                      ) {
  n <- length(x)
  
  # Initial guess for the median
  median_x <- sum(x * weights) / sum(weights)
  median_y <- sum(y * weights) / sum(weights)
  
  while (TRUE) {
    # Compute distances and inverse distances with weights
    distances <- sqrt((x - median_x)^2 + (y - median_y)^2)
    inverse_distances <- weights / distances
    
    inverse_distances[is.infinite(inverse_distances)] <- 0
    
    # Compute the new median with weighted distances
    new_median_x <- sum(x * inverse_distances) / sum(inverse_distances)
    new_median_y <- sum(y * inverse_distances) / sum(inverse_distances)
    
    # Check convergence
    if (sqrt((new_median_x - median_x)^2 + (new_median_y - median_y)^2)< epsilon) {
      break
    }
    
    median_x <- new_median_x
    median_y <- new_median_y
  }
  
  point_df <- data.frame(median_x, median_y,radius = NA, what = "geomedian")
  point_sf <- st_as_sf(point_df, coords = c("median_x", "median_y"),crs = st_crs(crs))
  
  return(point_sf)
}



### Find and plot VP Center and Circle, Center of Gravity and Geometric Median of an Abundance Map
measures_of_center <- function(abundance,
                               crs = 3035) {
  
  # Find Valeriepieris Circle
  print("Finding Valeriepieris circle:")
  v_circle <- valeriepieris_circle(abundance, 0.5, crs = crs)
  
  # Find Center of gravity with pelarrrp
  print('Finding cog')
  cog <- st_as_sf(abundance) %>%
    centre_of_gravity(weight="abundance") %>%
    mutate(radius = NA,
           what = "cog"
           ) %>%
    st_cast()
  
  # Find Weighted Geometric Median
  print('Finding gmedian')
  gmedian = geomedian(abundance$x, abundance$y, abundance$abundance, crs = crs)
  
  out <- rbind(v_circle$center,
               cog,
               gmedian
               ) %>%
    st_cast()
  return(out)
}


vp_profile <- function(abundance,f_values) {
  
  # R(1)
  vp_max <- valeriepieris_circle(abundance,1)
  R_1 <- vp_max$radius
  
  
  # VP_profile
  columns = c("f_value","center","norm_radius")
  profile = data.frame(matrix(nrow = length(f_values), ncol = length(columns)))
  colnames(profile) = columns
  
  print("Calculating VP-profile")
  for (i in 1:(length(f_values)-1)) {
    v_circle = valeriepieris_circle(abundance,f_values[i])
    profile$f_value[i] <- f_values[i]
    profile$center[i] <- st_as_sf(v_circle$center$geometry)
    profile$norm_radius[i] <- v_circle$radius/R_1
    print(paste(i+1,"/",length(f_values)))
  }
  
  profile$f_value[length(f_values)] <- 1
  profile$center[length(f_values)] <- st_as_sf(vp_max$center$geometry)
  profile$norm_radius[length(f_values)] <- 1
  
  return(profile)
}


