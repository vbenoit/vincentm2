################################################################################
### Description
# Auteurs: Matthieu AUTHIER, Vincent BENOIT
# Dernière modif: 2023-07-10
# R version 4.2.3 (2023-03-15 ucrt) -- "Shortstop Beagle"
# Copyright (C) 2023 The R Foundation for Statistical Computing
# Platform: x86_64-w64-mingw32/x64 (64-bit)
#
################################################################################

### Source file for functions

library(move)
library(tidyverse)
library(sismow)
library(sf)
library(sfheaders)
library(units)
library(pelarrp)

### Generate an abundance map from environmental covariates
make_distri <- function(sfdf,
                        grid,
                        month_num = 7,
                        mean_density = 0.1,
                        covname = c("Bathy", "Slope", "mSST"),
                        monotone = NULL,
                        n_knots = 10,
                        pos_patch = NULL,
                        sill = c(log(1.3) / 2, log(1.15) / 2),
                        amplitude = log(5) / 2,
                        range_coef = c(40000, 300000),
                        seed = 123
                        ) {
  set.seed(seed)
  ### remove na
  sfdf <- sfdf %>%
    select("ID", all_of(covname), "Year", "Month", "geom") %>%
    drop_na()
  
  ### standardize on whole dataset
  X <- sfdf %>%
    st_drop_geometry() %>%
    select(all_of(covname)) %>%
    scale() %>%
    as.data.frame()
  
  ### subset on month
  keep <- sfdf %>%
    mutate(rowID = 1:n()) %>%
    filter(Month == month_num) %>%
    pull(rowID)
  
  ### variable importance partitioning
  w <- rgamma(length(covname), 1.0, 1.0)
  w <- w / sum(w)
  sigma <- amplitude * sqrt(w) %>%
    sort(decreasing = TRUE)
  
  ### randomly generate relations between the covariables of interest and abundance
  # make bsplines and a graph
  Bgraph <- data.frame(cov_name = NULL, cov_value = NULL, y_value = NULL)
  B <- array(data = NA, dim = c(length(covname), length(keep)))
  ### deals with the kind of relationship
  w <- sapply(1:length(covname),
              function(j) {
                (-1)^(2 - rbinom(n_knots + 3, size = 1, prob = 0.5))
              }
  )
  
  if(!is.null(monotone)) {
    if(length(monotone) == length(covname)) {
      if(all(monotone %in% c("0", "+", "-"))) {
        for(j in 1:length(monotone)) {
          if(monotone[j] != "0") {
            w[, j] <- ifelse(monotone[j] == "-", -1, 1)
          }
        }
      } else {
        writeLines("'monotone' must be either '0' (non monotonous), '+' (non-decreasing) or '-' (non-increasing)")
      }
    } else {
      writeLines("length of arg. 'monotone' must be equal to that of arg. 'covname'")
    }
  }
  # coeff.
  beta <- sapply(1:length(covname),
                 function(j) {
                   out <- (rchisq(n_knots + 3, df = 2) * w[, j]) %>%
                     cumsum() %>%
                     scale() * sigma[j]
                 }
  )
  
  for(j in 1:length(covname)) {
    raw_X <- sfdf %>%
      pull(covname[j])
    
    x <- X %>%
      pull(covname[j]) %>%
      as.numeric()
    bsplines <- pelaStan::design_matrix(x = x,
                                        xl = min(x) - 0.05 * sd(x),
                                        xr = max(x) + 0.05 * sd(x),
                                        ndx = n_knots
    )
    B[j, ] <- bsplines$B[keep, ] %*% beta[, j]
    bsplines <- pelaStan::design_matrix(x = seq(min(x), max(x), length.out = 1e3),
                                        xl = min(x) - 0.05 * sd(x),
                                        xr = max(x) + 0.05 * sd(x),
                                        ndx = n_knots
    )
    Bgraph <- Bgraph %>%
      rbind(.,
            data.frame(cov_name = covname[j],
                       cov_value = mean(raw_X) + seq(min(x), max(x), length.out = 1e3) * sd(raw_X),
                       y_value = bsplines$B %*% beta[, j]
            )
      )
    
    rm(bsplines, x, raw_X)
  }; rm(j)
  
  out <- sfdf %>%
    filter(Month == month_num) %>%
    mutate(log_density = apply(B, 2, sum)) %>%
    st_as_sf()
  
  ### put back on regular grid using interpolation
  mod <- out %>%
    mutate(x = (st_centroid(.) %>%
                  st_coordinates(.) %>%
                  as.data.frame() %>%
                  pull(X)),
           y = (st_centroid(.) %>%
                  st_coordinates(.) %>%
                  as.data.frame() %>%
                  pull(Y))
    ) %>%
    st_drop_geometry() %>%
    mgcv::gam(log_density ~ te(x, y, bs = "cs"), data = .)
  
  ### this is the regular grid
  pred_data <- grid %>%
    mutate(x = (st_centroid(.) %>%
                  st_coordinates(.) %>%
                  as.data.frame() %>%
                  pull(X)),
           y = (st_centroid(.) %>%
                  st_coordinates(.) %>%
                  as.data.frame() %>%
                  pull(Y))
    )
  
  pred <- mod %>%
    mgcv::predict.gam(.,
                      newdata = pred_data %>%
                        st_drop_geometry(),
                      type = "response", se.fit = FALSE
    ) %>%
    as.numeric()
  
  ### distance matrix
  d <- grid %>%
    st_centroid() %>%
    st_distance() %>%
    drop_units()
  
  ### add 'nuggets' variance (to replace 'hot-spots')
  pepite <-  spheri_cov(d, 1, range_coef[1])
  epsilon <- rnorm(nrow(pepite))
  epsilon <- (pepite %*% matrix(sill[1] * epsilon, ncol = 1)) %>%
    drop()
  
  ### add spatial patch
  #large scale with spherical covariance
  if(!is.null(pos_patch)) {
    n_patch = nrow(pos_patch)
    
    patch <-  spheri_cov(d, 1, range_coef[2])
    loc_patch <- pos_patch %>%  #df converted in coord.
      st_as_sf(coords = c("x", "y"),
               crs = 4326 # WGS 84
      ) %>%
      st_transform(st_crs(grid)) # objective: to obtain 1 POINT (3242465 2644952)
    
    
    ## where are the patches?
    loc_all = as.vector(NULL)
    for (p in 1:n_patch) { #cell number location
      loc <- as.numeric(st_within(loc_patch[p,], grid))
      loc_all[p] <- loc
    }
    
    ## what are their neighbours?
    neighbours <- st_chess(sfdf = grid, chess_pawn = "queen")
    loca <- do.call('c', neighbours[loc_all])
    loca <- do.call('c', neighbours[loca])
    loca <- loca[!loca %in% loc_all]
    
    ## patch variance
    upsilon <- qnorm(runif(nrow(patch), min = 0.0, max = 0.5))
    upsilon[loc_all] <- qnorm(runif(length(loc_all), min = 0.8, max = 1.0))
    upsilon[loca] <- qnorm(runif(length(loca), min = 0.5, max = 0.8))
    
    ## spatial fields with patch
    upsilon <- (patch %*% matrix(sill[2] * scale(upsilon), ncol = 1)) %>%
      drop()
  } else {
    upsilon <- 0
  }
  
  epsilon <- epsilon + upsilon
  
  pred_data <- pred_data %>%
    mutate(density = mean_density * exp(pred + epsilon - 0.5 * mod$sig2^2),
           density = set_units(density, 'km^(-2)')
    ) %>%
    select(ID, x, y, density, geom) %>%
    st_as_sf()
  
  out <- list(distribution = pred_data,
              splines = Bgraph
  )
  
  return(out)
}





### Add new transect of coordinates "coordinates" to existing transect dataframe "transects"
### Generate a new set of individuals and observations on this new set of individuals along the new transect
### (Simulates different times for each resampling, overall density map is the same, but generated individuals change slightly)
### Repeat r times and return updated transect and detected dataframes
add_detections <- function(transects,coordinates,r,detected,map_obj,group_size,N_ind,truncation,tr_esw){
  # Adding a column to the dataframe to differentiate the transects generated by sismow and those added by hand
  new_trans <- transects
  
  new_detections <- detected
  
  # Create new transect of coordinates "coordinates"
  new_linestring <- sf_linestring(matrix(coordinates,
                                         2,
                                         2)
                                  )
  
  st_crs(new_linestring) <- crs
  
  
  # Segmentize new transect (re-using code from sismow)
  
  new_seg <- st_segmentize(new_linestring, dfMaxLength = set_units(2*truncation, "metres"))
  
  # geometry
  ggg <- st_geometry(new_seg)
  
  # check
  if (isFALSE(st_geometry_type(ggg) %in% c("LINESTRING"))) {
    stop("Input should be LINESTRING")
  }
  
  # loop to segmentize
  for (k in 1:length(st_geometry(ggg))) {
    # segment in each transect as sf multilinestring
    sub <- ggg[k]
    geom <- lapply(1:(length(st_coordinates(sub)[, 1]) - 1),
                   function(i)rbind(as.numeric(st_coordinates(sub)[i, 1:2]), as.numeric(st_coordinates(sub)[i + 1, 1:2]))) %>%
      st_multilinestring() %>%
      st_sfc()
    # create endgeom with the multilinestring for each transect
    if (k == 1) {endgeom <- geom}
    else {endgeom <- rbind(endgeom, geom)}
  }
  
  # sf object
  endgeom <- endgeom %>%
    st_sfc(crs = crs)
  
  # Multilinestring to linestring
  endgeom <- st_set_geometry(new_seg, endgeom) %>%
    st_cast("LINESTRING")
  
  # Add effort
  endgeom$effort <- st_length(endgeom)
  
  # Simulate observations over this transect r times with different individuals
  if(r != 0){ # if r = 0 do nothing and return transect and detected dataframes unchanged
    for (i in 1:r){
      # Matching transect/segments ID to the existing dataframe
      last_transect = new_trans[nrow(new_trans),]$transect
      # Matching object ID to the existing dataframe
      last_object_ID = detected[nrow(detected),]$object_ID
      
      # Creating a dataframe with the new segments
      new_seg <- data.frame(matrix(nrow=nrow(endgeom), ncol =5))
      colnames(new_seg)=c("transect","seg_ID","effort","geometry","sim")
      
      for (n in 1:nrow(endgeom)){
        new_seg$transect[n] = last_transect + 1
        new_seg$seg_ID[n] = paste(last_transect + 1, n, sep="-")
        new_seg$effort[n] = endgeom[n,]$effort
        new_seg$geometry[n] = endgeom[n,]$geometry
        new_seg$sim[n] = 0
      }
      
      new_seg$geometry <- new_seg$geometry %>% st_sfc(crs=crs)
      new_seg <- st_as_sf(new_seg)
      
      # Generate new individuals with the same density map (simulate different times of observation)
      new_ind <- simulate_ind(map_obj = map_obj,
                              mean_group_size = group_size,
                              N = N_ind,
                              crs=crs
      )
      
      # Simulate detections along the new transect with the new individuals and given esw
      new_obs <- simulate_obs(ind_obj = new_ind,
                              transect_obj = new_seg,
                              key = "hn",
                              g_zero = 1,
                              esw = tr_esw,
                              truncation = truncation,
                              crs = crs
      )
      
      
      # Add new detections to existing detected dataframe
      new_obs <- filter(new_obs, detected==1)
      
      if (nrow(new_obs) != 0){       # If no new observations, do nothing
        for (i in 1:length(new_obs$object_ID)){
          new_obs$object_ID[i] = last_object_ID + i
        }
        
        new_detections <- rbind(new_detections,new_obs)
      }
      
      # Add new transect to existing transects dataframe
      new_trans <- rbind(new_trans, st_as_sf(new_seg))
    }
  }

  return(list(new_trans=new_trans,new_detections=new_detections))
}

# Convert Cartesian to polar coordinates
cartesian_to_polar <- function(coords) {
  x <- coords[1]
  y <- coords[2]
  r <- sqrt(x^2 + y^2)/1000
  theta <- atan2(y, x) * (180 / pi)
  return(data.frame(distance = r, angle = theta))
}
