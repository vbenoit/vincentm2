lapply(c("tidyverse", "purrr", "sf", "units", "mgcv", "pelarrp"),
       library, character.only = TRUE
       )

### load environemental data
data(envGrid)
envGrid <- envGrid %>%
  st_as_sf(coords = c("X", "Y"), crs = 4326) %>%
  group_by(L1, L2) %>%
  summarise(do_union = FALSE) %>%
  st_cast("POLYGON") %>%
  ungroup() %>%
  st_transform(crs = 3035) %>%
  mutate(ID = 1:nrow(.)) %>%
  select(-L1, -L2) %>%
  st_cast()

data(env2019)
env2019 <- envGrid %>%
  left_join(.,
            env2019
            ) %>%
  st_cast()

# env2019 %>%
#   ggplot() +
#   geom_sf(aes(fill = mSST), color = NA) +
#   facet_wrap(~ Month, ncol = 4) +
#   scale_fill_viridis_c() +
#   theme_bw()

### grid
data(delmogesGrid)
delmogesGrid <- delmogesGrid %>%
  st_as_sf(coords = c("X", "Y"), crs = 4326) %>%
  group_by(L1, L2) %>%
  summarise(do_union = FALSE) %>%
  st_cast("POLYGON") %>%
  ungroup() %>%
  st_transform(crs = 3035) %>%
  mutate(ID = 1:n()) %>%
  select(-L1, -L2) %>%
  st_cast()

### generate a distribution
?make_distri
### only-increasing relationship with distance to coast
prey <- make_distri(sfdf = env2019,
                    grid = delmogesGrid,
                    mean_density = 1,
                    covname = c("dist2coast_km", "mSST"),
                    monotone = c("+", "0"),
                    n_hotspot = 0,
                    sill = log(1) / 2 * sqrt(c(0.5, 0.5)),
                    amplitude = log(3) /2,
                    range = c(15000, 50000),
                    seed = 19811021
                    )

prey$distribution %>%
  mutate(density = drop_units(density)) %>%
  ggplot() +
  geom_sf(aes(fill = density), color = NA) +
  scale_fill_viridis_c() +
  theme_bw()

prey$splines %>%
  ggplot(aes(x = cov_value, y = y_value)) +
  geom_line() +
  facet_wrap(~ cov_name, scale = "free_x") +
  theme_bw()

### only-decreasing relationship with distance to coast
prey <- make_distri(sfdf = env2019,
                    grid = delmogesGrid,
                    mean_density = 1,
                    covname = c("dist2coast_km", "mSST"),
                    monotone = c("-", "0"),
                    n_hotspot = 0,
                    sill = log(1) / 2 * sqrt(c(0.5, 0.5)),
                    amplitude = log(3) /2,
                    range = c(15000, 50000),
                    seed = 19811021
                    )

prey$distribution %>%
  mutate(density = drop_units(density)) %>%
  ggplot() +
  geom_sf(aes(fill = density), color = NA) +
  scale_fill_viridis_c() +
  theme_bw()

prey$splines %>%
  ggplot(aes(x = cov_value, y = y_value)) +
  geom_line() +
  facet_wrap(~ cov_name, scale = "free_x") +
  theme_bw()

### random
prey <- make_distri(sfdf = env2019,
                    grid = delmogesGrid,
                    mean_density = 1,
                    covname = c("dist2coast_km", "mSST"),
                    monotone = c("0", "0"),
                    n_hotspot = 0,
                    sill = log(1) / 2 * sqrt(c(0.5, 0.5)),
                    amplitude = log(3) /2,
                    range = c(15000, 50000),
                    seed = 19811021
                    )

prey$distribution %>%
  mutate(density = drop_units(density)) %>%
  ggplot() +
  geom_sf(aes(fill = density), color = NA) +
  scale_fill_viridis_c() +
  theme_bw()

prey$splines %>%
  ggplot(aes(x = cov_value, y = y_value)) +
  geom_line() +
  facet_wrap(~ cov_name, scale = "free_x") +
  theme_bw()

### hotspots: 0, just noise
prey <- make_distri(sfdf = env2019,
                    grid = delmogesGrid,
                    mean_density = 1,
                    covname = c("dist2coast_km", "mSST"),
                    n_hotspot = 0,
                    sill = log(1.2) / 2 * sqrt(c(1, 0)),
                    amplitude = log(1) /2,
                    range = c(15000, 50000),
                    seed = 19811021
                    )

prey$distribution %>%
  mutate(density = drop_units(density)) %>%
  ggplot() +
  geom_sf(aes(fill = density), color = NA) +
  scale_fill_viridis_c() +
  theme_bw()

### hotspots: 10
prey <- make_distri(sfdf = env2019,
                    grid = delmogesGrid,
                    mean_density = 1,
                    covname = c("dist2coast_km", "mSST"),
                    n_hotspot = 10,
                    sill = log(1.2) / 2 * sqrt(c(1, 0)),
                    amplitude = log(1) /2,
                    range = c(15000, 50000),
                    seed = 19811021
                    )

prey$distribution %>%
  mutate(density = drop_units(density)) %>%
  ggplot() +
  geom_sf(aes(fill = density), color = NA) +
  scale_fill_viridis_c() +
  theme_bw()

### hotspots: 10 + long-range
prey <- make_distri(sfdf = env2019,
                    grid = delmogesGrid,
                    mean_density = 1,
                    covname = c("dist2coast_km", "mSST"),
                    n_hotspot = 10,
                    sill = log(1.2) / 2 * sqrt(c(0.5, 0.5)),
                    amplitude = log(1) /2,
                    range = c(15000, 50000),
                    seed = 19811021
                    )

prey$distribution %>%
  mutate(density = drop_units(density)) %>%
  ggplot() +
  geom_sf(aes(fill = density), color = NA) +
  scale_fill_viridis_c() +
  theme_bw()

### add covariate effects
prey <- make_distri(sfdf = env2019,
                    grid = delmogesGrid,
                    mean_density = 1,
                    covname = c("dist2coast_km", "mSST"),
                    monotone = c("-", "0"),
                    n_hotspot = 10,
                    sill = log(1.2) / 2 * sqrt(c(0.5, 0.5)),
                    amplitude = log(1.5) /2,
                    range = c(15000, 50000),
                    seed = 19811021
                    )

prey$distribution %>%
  mutate(density = drop_units(density)) %>%
  ggplot() +
  geom_sf(aes(fill = density), color = NA) +
  scale_fill_viridis_c() +
  theme_bw()

### more weights on hotspots
prey <- make_distri(sfdf = env2019,
                    grid = delmogesGrid,
                    mean_density = 1,
                    covname = c("dist2coast_km", "mSST"),
                    monotone = c("-", "0"),
                    n_hotspot = 10,
                    sill = log(1.2) / 2 * sqrt(c(0.8, 0.2)),
                    amplitude = log(1.5) /2,
                    range = c(15000, 50000),
                    seed = 19811021
                    )

prey$distribution %>%
  mutate(density = drop_units(density)) %>%
  ggplot() +
  geom_sf(aes(fill = density), color = NA) +
  scale_fill_viridis_c() +
  theme_bw()

### less effect on covariates
prey <- make_distri(sfdf = env2019,
                    grid = delmogesGrid,
                    mean_density = 1,
                    covname = c("dist2coast_km", "mSST"),
                    monotone = c("-", "0"),
                    n_hotspot = 10,
                    sill = log(1.5) / 2 * sqrt(c(0.8, 0.2)),
                    amplitude = log(1.2) /2,
                    range = c(15000, 50000),
                    seed = 19811021
                    )

prey$distribution %>%
  mutate(density = drop_units(density)) %>%
  ggplot() +
  geom_sf(aes(fill = density), color = NA) +
  scale_fill_viridis_c() +
  theme_bw()

### to have a smooth map, put more weights on covariate effects, then on sill[2] and last on sill[1]
prey <- make_distri(sfdf = env2019,
                    grid = delmogesGrid,
                    mean_density = 1,
                    covname = c("dist2coast_km", "mSST"),
                    monotone = c("-", "0"),
                    n_hotspot = 10,
                    sill = log(1.5) / 2 * sqrt(c(0.4, 0.6)),
                    amplitude = log(3) /2,
                    range = c(15000, 50000),
                    seed = 19811021
                    )

 prey$distribution %>%
  mutate(density = drop_units(density)) %>%
  ggplot() +
  geom_sf(aes(fill = density), color = NA) +
  scale_fill_viridis_c() +
  theme_bw()

### compare with no additional hotspots: notice the change of scale for density
prey <- make_distri(sfdf = env2019,
                    grid = delmogesGrid,
                    mean_density = 1,
                    covname = c("dist2coast_km", "mSST"),
                    monotone = c("-", "0"),
                    n_hotspot = 0,
                    sill = log(1.5) / 2 * sqrt(c(0.4, 0.6)),
                    amplitude = log(3) /2,
                    range = c(15000, 50000),
                    seed = 19811021
                    )

prey$distribution %>%
  mutate(density = drop_units(density)) %>%
  ggplot() +
  geom_sf(aes(fill = density), color = NA) +
  scale_fill_viridis_c() +
  theme_bw()

